<?php



if (!(version_compare(PHP_VERSION, "5.3") >= 0)) {
  throw new Exception('Podio PHP library requires PHP 5.3 or higher.');
}

include_once "library/FreshBooks/Client.php";

require_once 'lib/Podio.php';
require_once 'lib/PodioResponse.php';
require_once 'lib/PodioOAuth.php';
require_once 'lib/PodioError.php';
require_once 'lib/PodioCollection.php';
require_once 'lib/PodioObject.php';
require_once 'lib/PodioLogger.php';

require_once 'models/PodioFieldCollection.php'; // Included first because other models inherit from this
require_once 'models/PodioAction.php';
require_once 'models/PodioActivity.php';
require_once 'models/PodioApp.php';
require_once 'models/PodioAppField.php';
require_once 'models/PodioAppFieldCollection.php';
require_once 'models/PodioAppMarketShare.php';
require_once 'models/PodioBatch.php';
require_once 'models/PodioByLine.php';
require_once 'models/PodioCalendarEvent.php';
require_once 'models/PodioCalendarMute.php';
require_once 'models/PodioComment.php';
require_once 'models/PodioContact.php';
require_once 'models/PodioConversation.php';
require_once 'models/PodioConversationMessage.php';
require_once 'models/PodioConversationParticipant.php';
require_once 'models/PodioEmbed.php';
require_once 'models/PodioFile.php';
require_once 'models/PodioForm.php';
require_once 'models/PodioGrant.php';
require_once 'models/PodioHook.php';
require_once 'models/PodioImporter.php';
require_once 'models/PodioIntegration.php';
require_once 'models/PodioItem.php';
require_once 'models/PodioItemCollection.php';
require_once 'models/PodioItemDiff.php';
require_once 'models/PodioItemField.php';
require_once 'models/PodioItemFieldCollection.php';
require_once 'models/PodioItemRevision.php';
require_once 'models/PodioLinkedAccountData.php';
require_once 'models/PodioNotification.php';
require_once 'models/PodioNotificationContext.php';
require_once 'models/PodioNotificationGroup.php';
require_once 'models/PodioOrganization.php';
require_once 'models/PodioOrganizationMember.php';
require_once 'models/PodioQuestion.php';
require_once 'models/PodioQuestionAnswer.php';
require_once 'models/PodioQuestionOption.php';
require_once 'models/PodioRating.php';
require_once 'models/PodioRecurrence.php';
require_once 'models/PodioReference.php';
require_once 'models/PodioReminder.php';
require_once 'models/PodioSearchResult.php';
require_once 'models/PodioSpace.php';
require_once 'models/PodioSpaceMember.php';
require_once 'models/PodioStatus.php';
require_once 'models/PodioStreamObject.php';
require_once 'models/PodioSubscription.php';
require_once 'models/PodioTag.php';
require_once 'models/PodioTagSearch.php';
require_once 'models/PodioTask.php';
require_once 'models/PodioTaskLabel.php';
require_once 'models/PodioUser.php';
require_once 'models/PodioUserMail.php';
require_once 'models/PodioUserStatus.php';
require_once 'models/PodioVia.php';
require_once 'models/PodioView.php';
require_once 'models/PodioVoting.php';
require_once 'models/PodioWidget.php';

Podio::setup('company-zwab0v', 'FTtHCGrnwewInWfEiKcJj8uJZqiW9GnBYEeV886K1PwFkPZOgqXgav1BujKmUSoM');

try {
  Podio::authenticate_with_app("13093877", "ea9c359d861a418eb755fb3bd9dd1aca");

// Authentication was a success, now you can start making API calls.

}
catch (PodioError $e) {
// Something went wrong. Examine $e->body['error_description'] for a description of the error.
}

// Check if POST data is available
	if (isset($_POST) && !empty($_POST)){ 
	$post_dump = print_r($_POST, TRUE);
	$fpost = fopen('post.txt', 'a');
	fwrite($fpost, $post_dump);
	fclose($fpost);
		
	// Hook verifier	
		if ($_POST['type'] == 'hook.verify') {
	// Validate the webhook
			PodioHook::validate($_POST['hook_id'], array('code' => $_POST['code']));
		}
		
	// Get the item info from newly created item
		if ($_POST['type'] == 'item.create') {
			// Get the basic item info
			$item = PodioItem::get_basic($_POST['item_id']);
			
			$ItemID = $_POST['item_id'];

			// Get the field with the external_id=sample-external-id
			$field_id = 'title';

			$Company_name = $item->fields[$field_id]->values;
			
			$field_id = 'address';
			$Address = $item->fields[$field_id]->values;
			$field_id = 'city';
			$City = $item->fields[$field_id]->values;
			$field_id = 'state';
			$State = $item->fields[$field_id]->values;
			$field_id = 'zip-code-2';
			$ZipCode = $item->fields[$field_id]->values;
			$field_id = 'decision-maker-email';
			$DMEmail = $item->fields[$field_id]->values;
			$EmailParsed = $DMEmail[0]["value"];
			$field_id = 'contact';
			$collection = $item->fields[$field_id]->values;

			foreach ($collection as $referenced_item) {
			  $PrimaryContact = $referenced_item->title;
			}
			
			$ContactArray = preg_split("/ /",$PrimaryContact);
			
			$DMFirst = $ContactArray[0];
			$DMlast = $ContactArray[1];
			
			// place all attributes into single array (for debugging)
			$array_test = array($Company_name,$Address,$City,$State,$ZipCode,$EmailParsed,$DMFirst,$DMlast);
			
			$req_dump = print_r($array_test, TRUE);
			$fp = fopen('fieldtester.txt', 'a');
			fwrite($fp, $req_dump);
			fclose($fp);

			
			
			//API url and token from freshbooks.com
			$url = "https://scalaranalytics.freshbooks.com/api/2.1/xml-in";
			$token = "3a0b1776a2bec3ab79e46d78e38e3317";

			//init singleton FreshBooks_HttpClient
			FreshBooks_HttpClient::init($url,$token);

			//new Client object
			$client = new FreshBooks_Client();
			$client->email = "$EmailParsed";
			$client->firstName = "$DMFirst";
			$client->lastName = "$DMlast";
			$client->organization = $Company_name;
			$client->pStreet1 = $Address;
			$client->pCity = $City;
			$client->pState = $State;
			$client->pCode = $ZipCode;
			

			//try to get client with client_id 3
			if(!$client->create()){
			//no data - read error
				echo $client->lastError;
			}
			else{
			//investigate populated data

			$req_dump = print_r($client->clientId, TRUE);
			$fp = fopen('fieldtester.txt', 'a');
			fwrite($fp, $req_dump);
			fclose($fp);
			
			$req_dump = print_r($client, TRUE);
			$fp = fopen('fieldtester.txt', 'a');
			fwrite($fp, $req_dump);
			fclose($fp);
			
			
			$FreshBooks_ID = $client->clientId;
			
			$FieldID = "101524837";
			
			PodioItemField::update( $ItemID, $FieldID, array($FreshBooks_ID));
					
			}
		}
		
			
	}
	


?>


